-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here

ancho = display.contentWidth
height = display.contentHeight

display.newRect(ancho*0.125, height*0.125, ancho*0.25, height*0.25)
display.newRect(ancho*0.125, height*0.5, ancho*0.25, height*0.25)
display.newRect(ancho*0.125, height*0.875, ancho*0.25, height*0.25)

display.newRect(ancho*0.5, height*0.125, ancho*0.25, height*0.25)
display.newRect(ancho*0.5, height*0.5, ancho*0.25, height*0.25)
display.newRect(ancho*0.5, height*0.875, ancho*0.25, height*0.25)

display.newRect(ancho*0.875, height*0.125, ancho*0.25, height*0.25)
display.newRect(ancho*0.875, height*0.5, ancho*0.25, height*0.25)
display.newRect(ancho*0.875, height*0.875, ancho*0.25, height*0.25)

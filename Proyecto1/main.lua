-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here

print("holiwis")

ancho = display.contentWidth
height = display.contentHeight

display.newImage( "chimpukomon.jpg", ancho*0.5, height*0.5, fullResolution )
function make_bear(a,b,c)
  local oso = display.newGroup()

  local body = display.newCircle(oso,ancho*0.5,height*0.5,height*0.15)
  local head = display.newCircle(oso,ancho*0.5,height*0.285,height*0.075)
  local left_leg = display.newCircle(oso,ancho*0.66,height*0.66,30)
  local right_leg = display.newCircle(oso,ancho*0.34,height*0.66,30)
  local left_arm = display.newCircle(oso,ancho*0.74,height*0.4,30)
  local right_arm = display.newCircle(oso,ancho*0.26,height*0.4,30)
  local left_ear = display.newCircle(oso,ancho*0.6,height*0.23,20)
  local right_ear = display.newCircle(oso,ancho*0.4,height*0.23,20)

  body:setFillColor(a,b,c,1)
  head:setFillColor(a,b,c,1)
  left_leg:setFillColor(a,b,c,1)
  right_leg:setFillColor(a,b,c,1)
  left_arm:setFillColor(a,b,c,1)
  right_arm:setFillColor(a,b,c,1)
  left_ear:setFillColor(a,b,c,1)
  right_ear:setFillColor(a,b,c,1)

  return oso
end

local new_oso = make_bear(210/255,105/255,30/255)
local new_oso2 = make_bear(0,0,0)

new_oso2.x = 50

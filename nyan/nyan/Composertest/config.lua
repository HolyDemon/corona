application =
{

	content =
	{
		width = 360,
		height = 640, 
		scale = "letterBox",
		fps = 30,

		--[[
		imageSuffix =
		{
			    ["@2x"] = 2,
		},
		--]]
	},

	--[[
	-- Push notifications
	notification =
	{
		iphone =
		{
			types =
			{
				"badge", "sound", "alert", "newsstand"
			}
		}
	},
	--]]
}

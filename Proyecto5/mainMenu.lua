
----------------------------------------------------------------------------------
--
-- SceneMainMenu.lua
--
----------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- createScene
function scene:create( event )

	print "SceneMainMenu createScene"
	local group = self.view


end

-- enterScene
function scene:show( event )

	print "SceneMainMenu enterScene"
	local group = self.view
	local phase = event.phase

	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then

        local bg = display.newImage("images/main_bg.png",0,0,true)
        bg.anchorX = 0
        bg.anchorY = 0
        local title = display.newImage("images/main_title.png",-300,-300,true)
        local play = display.newImage("images/main_play.png",_W/2,_H,true)
        local sSound = audio.loadSound ("sounds/sound_select.wav")
        function fTitle ()

            local tTitle = transition.to(title ,{time = 1000, y = 150,x = 400})

        end




        local tPlay = transition.to(play ,{time = 1000, y=_H*0.5, onComplete = fTitle})

        function fPlay(event)
            if(event.phase == "began") then
                composer.gotoScene("mision1","fade",700)
                audio.stop(1) -- detenemos el canal
                audio.play(sSound)
            end
        end
        play:addEventListener("touch", fPlay)
        group:insert(bg)
        group:insert(title)
        group:insert(play)

    end


end

-- exitScene
function scene:hide( event )

	print "SceneMainMenu exitScene"
	composer.removeScene ( "mainMenu" )
	local group = self.view

end

-- destroyScene
function scene:destroy( event )

	print "SceneMainMenu destroyScene"
	local group = self.view

end

-- sceneEventListeners
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


return scene

-- end

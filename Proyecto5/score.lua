----------------------------------------------------------------------------------
--
-- SceneMision1.lua
--
----------------------------------------------------------------------------------

local composer = require ("composer")
local scene = composer.newScene()

require "physics"
system.activate("multitouch")
-- createScene
function scene:create( event )

	print "SceneMision1 createScene"
	local group = self.view

end


-- enterScene
function scene:show( event )

	print "SceneMision1 enterScene"
	local group = self.view
	local phase = event.phase

	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then

        local bg = display.newImage("images/main_bg.png",0,0,true)
        bg.anchorX = 0
        bg.anchorY = 0
        local sCat = audio.loadSound("sounds/sound_meow.wav")
        score = 0
        timeLeft = 30

        local options =
        {
            --required parameters
            width = 32,
            height = 32,
            numFrames = 18,
        }

        local imageSheet = graphics.newImageSheet( "tux_sprite.png", options )


        local sequenceData =
        {
					  {
							  name='chill',
								start=1,
								count=3,
								time=1000,
								loopCount = 0,
								loopDirection = 'bounce'
						},
						{
							  name='walking',
								start=11,
								count=6,
								time=1000,
								loopCount = 0,
								loopDirection = 'forward'
						},
						{
							  name='flapping',
								start=17,
								count=2,
								time=1000,
								loopCount = 0,
								loopDirection = 'forward'
						}
        }


        physics.start()
        physics.setDrawMode( "normal" )
        physics.setGravity(0,0)-- -3.8
        group:insert(bg)

        function nyanRemove(event)

            if (event.phase == "ended") then

                event.target:removeSelf()

            end

        end

        function nyanTouch(event)

            event.target:setSequence( "flapping" )
            event.target:play()
            audio.play(sCat)

        end

        function spawnCat()

            nyanSprite =  display.newSprite( imageSheet, sequenceData )
						nyanSprite:scale(10,10)
            nyanSprite.x = math.random(20,_W-50)
            nyanSprite.y = _H * 0.5
            nyanSprite:setSequence( "chill" )
            nyanSprite:play()
            physics.addBody(nyanSprite, "dynamic", { friction = 0, bounce = 0 })
            group:insert(nyanSprite)
            nyanSprite:addEventListener("touch", nyanTouch)
            --nyanSprite:addEventListener("sprite", nyanRemove)

        end

        spawnCat()
    --	timer.performWithDelay( variablesStash.hitTime , variablesStash.onHit, 1 )

    end

end

-- exitScene
function scene:hide( event )

	print "SceneMision1 exitScene"
	local group = self.view

end

-- destroyScene
function scene:destroy( event )

	print "SceneMision1 destroyScene"
	local group = self.view

end

-- sceneEventListeners
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene

-- end
